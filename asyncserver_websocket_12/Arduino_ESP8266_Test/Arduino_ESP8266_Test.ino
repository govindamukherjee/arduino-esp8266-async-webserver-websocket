#include <DS3231.h>
#include <Wire.h>
#include <ArduinoJson.h>
#include <FS.h>

DS3231 Clock;
RTClib RTC;
DateTime now;
EspClass myESP;

byte j;
bool on = false;

//const byte pin_led = 16;      //D0 NODE MCU LED
const byte pin_esp_led = 2;   //D4 ESP LED
const byte interruptPin = 14;  //D5
//const byte adcPin = A0; //A0 for HV
//const byte buttonPin = 4; //D2 PUSH BUTTON FOR MODE

int int_cr = 0;

byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

bool Century=false;
bool h12;
bool PM;
unsigned long oldSec = 0;
unsigned long newSec = 0;

String dt = "2020-05-02 14:41:30";
String dtStamp = "200502144130";

void splitString()
{
  Serial.println("Split String Example");
  String input = "123,456";
  int firstVal, secondVal;
  
  for (int i = 0; i < input.length(); i++) {
    if (input.substring(i, i+1) == ",") {
      firstVal = input.substring(0, i).toInt();
      secondVal = input.substring(i+1).toInt();
      break;
    }
  }
  Serial.println(input);
  Serial.println(firstVal);
  Serial.println(secondVal);
}
void customSplitString()
{
  Serial.println("Custom Split String Example");
  String input = "20001#a#56";
  String srNo, opCode;
  int opValue;

  srNo = input.substring(0, 5);
  opCode = input.substring(6, 7);
  opValue = input.substring(8).toInt();
  
 
  Serial.println(input);
  Serial.println(srNo);
  Serial.println(opCode);
  Serial.println(opValue);
}

void jsonStringParse()
{
  Serial.println("Json String Parse Example");

  StaticJsonDocument<200> doc;

  String jsonString = "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, jsonString);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  String sensor = doc["sensor"];
  long time = doc["time"];
  double latitude = doc["data"][0];
  double longitude = doc["data"][1];

  // Print values.
  Serial.println(sensor);
  Serial.println(time);
  Serial.println(latitude, 6);
  Serial.println(longitude, 6);
}

void customJsonStringParse()
{
  Serial.println("Custom Json String Parse Example");

  StaticJsonDocument<200> doc;

  String jsonString = "{\"eff\":25,\"bkgCPS\":5,\"bkgSec\":1,\"samSec\":1,\"cl\":95}";

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, jsonString);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  uint8_t eff = doc["eff"];
  uint8_t cl = doc["cl"];
  unsigned int bkgCPS = doc["bkgCPS"];
  unsigned int bkgSec = doc["bkgSec"];
  unsigned int samSec = doc["samSec"];

  // Print values.
  Serial.println(eff);
  Serial.println(cl);
  Serial.println(bkgCPS);
  Serial.println(bkgSec);
  Serial.println(samSec);
}

const char *logFile = "/log20001.json";
void sendLogData()
{
  String fileData = "";
  Serial.println("Send Log file");
  File file = SPIFFS.open(logFile, "r");
  if (file) 
  {
    char currentChar = 'a';
    bool startRecording = false;
    while(file.available())
    {
      currentChar = (char)file.read();
      if(currentChar == '#')
      {
        startRecording = false;
        fileData = "20001#x#u#" + fileData;
        Serial.println(fileData);
        fileData = "";
        continue;
        
      }
      else if(currentChar == '$')
      {
        startRecording = true;
        continue;        
      }

      if(startRecording)
      {
        fileData = fileData + currentChar;
      }
            
    }
    file.close();
  }
  else
  {
    Serial.println("Error opening file for reading");
    return;
  }
}

void setDateTime(String eventID)
{
  Year = eventID.substring(0, 2).toInt();
  Month = eventID.substring(2, 4).toInt();
  Date = eventID.substring(4, 6).toInt();
  Hour = eventID.substring(6, 8).toInt();
  Minute = eventID.substring(8, 10).toInt();
  Second = eventID.substring(10, 12).toInt();
  
  Clock.setYear(Year);
  Clock.setMonth(Month);
  Clock.setDate(Date);
  Clock.setHour(Hour);
  Clock.setMinute(Minute);
  Clock.setSecond(Second);
}

void ICACHE_RAM_ATTR handleInterrupt() 
{
    int_cr++;
}

void setup() 
{
  //pinMode(pin_led, OUTPUT);
  //pinMode(pin_esp_led, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  //pinMode(buttonPin, INPUT_PULLUP);
  
  Serial.begin(115200);
  delay(10);

  Wire.begin();
  delay(10);
  
  Clock.setClockMode(false);  // set to 24h
  Clock.enable32kHz(true);
  Clock.enableOscillator(true, false, 0);

  now = RTC.now();
  oldSec = now.unixtime();

  Year = 83;
  Month = 1;
  Date = 25;
  DoW = 6;
  Hour = 22;
  Minute = 32;
  Second = 33;
  setDateTime("200503212830");

  // Initialize SPIFFS
  if(!SPIFFS.begin())
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, FALLING);
  delay(10);
}

void loop() 
{
  // put your main code here, to run repeatedly:
  Serial.println(String(int_cr));
  Serial.println(String(Clock.getTemperature()));
  int_cr = 0;

  Year = Clock.getYear();
  Month = Clock.getMonth(Century);
  Date = Clock.getDate();
  DoW = Clock.getDoW();
  Hour = Clock.getHour(h12, PM);
  Minute = Clock.getMinute();
  Second = Clock.getSecond();

  dt = String(2000+Year, DEC);
  dt = dt + "-";
  dt = dt + String(Month, DEC);
  dt = dt + "-";
  dt = dt + String(Date, DEC);
  dt = dt + " ";
  dt = dt + String(Hour, DEC);
  dt = dt + ":";
  dt = dt + String(Minute, DEC);
  dt = dt + ":";
  dt = dt + String(Second, DEC);
  
  Serial.println(dt);

  dtStamp = String(Year, DEC);
  if(Month < 10)
  {
    dtStamp = dtStamp + "0" + String(Month, DEC);
  }
  else
  {
    dtStamp = dtStamp + String(Month, DEC);
  }

  if(Date < 10)
  {
    dtStamp = dtStamp + "0" + String(Date, DEC);
  }
  else
  {
    dtStamp = dtStamp + String(Date, DEC);
  }

  if(Hour < 10)
  {
    dtStamp = dtStamp + "0" + String(Hour, DEC);
  }
  else
  {
    dtStamp = dtStamp + String(Hour, DEC);
  }

  if(Minute < 10)
  {
    dtStamp = dtStamp + "0" + String(Minute, DEC);
  }
  else
  {
    dtStamp = dtStamp + String(Minute, DEC);
  }

  if(Second < 10)
  {
    dtStamp = dtStamp + "0" + String(Second, DEC);
  }
  else
  {
    dtStamp = dtStamp + String(Second, DEC);
  }
  
  Serial.println(dtStamp);
  
  Serial.println(Century);
  Serial.println(h12);
  Serial.println(PM);
  Serial.println(DoW);

  
    
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();

  now = RTC.now();
  Serial.print("since midnight 1/1/1970= ");
  newSec = now.unixtime();
  Serial.print(oldSec);
  Serial.print(" new sec = ");
  Serial.print(newSec);
  Serial.print(" difference: ");
  Serial.println(newSec - oldSec);
  oldSec = newSec;

  myESP.getResetInfo();
  myESP.getResetReason();

  sendLogData();
  Serial.println();
  
  delay(1000);                  
}
